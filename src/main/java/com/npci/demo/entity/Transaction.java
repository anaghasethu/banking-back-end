package com.npci.demo.entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transaction")

public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int trans_id;

	private int c_id;

	private Date trans_date;

	private float trans_amount;

	private int trans_c_id;

	private int type;

	public Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Transaction(int trans_id, int c_id, Date trans_date, float trans_amount, int trans_c_id, int type) {
		super();
		this.trans_id = trans_id;
		this.c_id = c_id;
		this.trans_date = trans_date;
		this.trans_amount = trans_amount;
		this.trans_c_id = trans_c_id;
		this.type = type;
	}

	public int getTrans_id() {
		return trans_id;
	}

	public void setTrans_id(int trans_id) {
		this.trans_id = trans_id;
	}

	public int getC_id() {
		return c_id;
	}

	public void setC_id(int c_id) {
		this.c_id = c_id;
	}

	public Date getTrans_date() {
		return trans_date;
	}

	public void setTrans_date(Date trans_date) {
		this.trans_date = trans_date;
	}

	public float getTrans_amount() {
		return trans_amount;
	}

	public void setTrans_amount(float trans_amount) {
		this.trans_amount = trans_amount;
	}

	public int getTrans_c_id() {
		return trans_c_id;
	}

	public void setTrans_c_id(int trans_c_id) {
		this.trans_c_id = trans_c_id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Transaction [trans_id=" + trans_id + ", c_id=" + c_id + ", trans_date=" + trans_date + ", trans_amount="
				+ trans_amount + ", trans_c_id=" + trans_c_id + ", type=" + type + "]";
	}
}
