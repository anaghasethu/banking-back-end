package com.npci.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.npci.demo.dao.BankingDao;
import com.npci.demo.dao.TransactionDao;
import com.npci.demo.entity.Customers;
import com.npci.demo.entity.Transaction;
import com.npci.demo.exception.ResourceNotFoundException;
import com.npci.demo.repository.CustomersRepository;
import com.npci.demo.response.TransDetails;
import com.npci.demo.response.TransSum;
import com.npci.demo.response.TransactionResponse;
import com.npci.demo.service.BankingService;

@Service
public class BankingServiceImpl implements BankingService {
	@Autowired
	CustomersRepository customerRepository;
	@Autowired
	TransactionDao transactionRepository;
	@Autowired
	BankingDao bankingDao;
	
	
	@Override
	public List<Customers> getAllCustomers() {
		// TODO Auto-generated method stub
		return (List<Customers>) customerRepository.findAll();
	}

	@Override
	public Customers getById(int c_id) {
		// TODO Auto-generated method stub
		return customerRepository.findById(c_id).orElseThrow(() -> new ResourceNotFoundException("User not found"));

	}

	@Override
	public List<Customers> getbyLoc(String address) {
		// TODO Auto-generated method stub
		return (List<Customers>) customerRepository.getbyLoc(address);
	}

	@Override
	public Customers addCustomer(Customers customers) {
		// TODO Auto-generated method stub
		return customerRepository.save(customers);
	}

	@Override
	public String deleteCustomer(int c_id) {
		// TODO Auto-generated method stub
		Customers customer = null;
		String message = null;
		try {
			customer = customerRepository.findById(c_id).orElse(null);
			if (customer == null) {
				message = "Customer not available";
			} else {
				customerRepository.deleteById(c_id);
				message = "Deleted successfully..!";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
		}

		return message;
	}

	@Override
	public List<TransactionResponse> getTransDetails() {
		// TODO Auto-generated method stub
		return (List<TransactionResponse>) bankingDao.getTransDetail();
	}

	@Override
	public List<TransSum> getTransSum() {
		// TODO Auto-generated method stub
		return (List<TransSum>) bankingDao.getTransactionSum();
	}

	@Override
	public List<Customers> getAllCustomersBal() {
		// TODO Auto-generated method stub
		return (List<Customers>) customerRepository.findAll();
	}

	@Override
	public List<TransDetails> getTransBal() {
		// TODO Auto-generated method stub
		return bankingDao.getTransBal();
	}

	@Override
	public List<TransDetails> getTop() {
		// TODO Auto-generated method stub
		return bankingDao.getTop();
	}

	@Override
	public void addTransaction(Transaction transaction) throws Exception{
		// TODO Auto-generated method stub
		System.out.println("transaction");
		int c_id = transaction.getC_id();
		Customers entity = customerRepository.getById(c_id);
//		int trans_id = transaction.getTrans_id();
//		Transaction trans = transactionRepository.getById(trans_id);
//		
//		int trans_c_id = transaction.getTrans_c_id();
//		
		float customer_balance = entity.getBalance();
		float trans_amount = transaction.getTrans_amount();
		
		if(transaction.getType() == 1) {
			if (customer_balance >= trans_amount) {
				entity.setBalance(customer_balance - trans_amount);
				customerRepository.save(entity);
//				trans.setTrans_id(trans_id + 1);
//				trans.setC_id(trans_c_id);
				
				transactionRepository.save(transaction);

				int to_c_id = transaction.getTrans_c_id();
				Customers entity2 = customerRepository.getById(to_c_id);
				entity2.setBalance(entity2.getBalance() + trans_amount);
				customerRepository.save(entity2);
			} else {
				throw new Exception("Insufficient balance");
			}
		}
		//credit
		else if(transaction.getType() == 2) {
			if (customer_balance >= trans_amount) {
				entity.setBalance(customer_balance + trans_amount);
				customerRepository.save(entity);
//				trans.setTrans_id(trans_id + 1);
//				trans.setC_id(trans_c_id);
				
				transactionRepository.save(transaction);

				int to_c_id = transaction.getTrans_c_id();
				Customers entity2 = customerRepository.getById(to_c_id);
				entity2.setBalance(entity2.getBalance() - trans_amount);
				customerRepository.save(entity2);
			} else {
				throw new Exception("Insufficient balance");
			}
		}
		else {
			throw new Exception("Kindly check the type of transaction");
		}
		
	}
}
